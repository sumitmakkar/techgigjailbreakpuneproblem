#include<iostream>
#include<vector>

using namespace std;

class Engine
{
    private:
	   int jump;
	   int slip;
	   int numOfWalls;
	   vector<int> wallsHeight;
    
    public:
	   Engine(int j , int s , int n , vector<int> h)
	   {
           jump        = j;
           slip        = s;
           numOfWalls  = n;
           wallsHeight = h;
       }
    
	   int countNumberOfJumpsRequired()
	   {
           int count = 0;
           for(int i = 0 ; i < numOfWalls ; i++)
           {
               int   singleWallIntCount    = (int)((wallsHeight[i] - jump) / (jump - slip));
               float singleWallFloatCount  = ((float)wallsHeight[i] - (float)jump) / ((float)jump - (float)slip);
               int   singleWallCount       = (singleWallFloatCount - singleWallIntCount) > 0 ? singleWallIntCount + 2 : singleWallIntCount + 1;
               count                      += singleWallCount;
           }
           return count;
       }
};

int main()
{
    int jump;
    int slip;
    int numOfWalls;
    cin>>jump>>slip>>numOfWalls;
    vector<int> wallsHeight(numOfWalls);
    for(int i = 0 ; i < numOfWalls ; i++)
    {
        cin>>wallsHeight[i];
    }
    Engine e = Engine(jump , slip , numOfWalls , wallsHeight);
    cout<<e.countNumberOfJumpsRequired()<<endl;
    return 0;
}
